/**
 * Copyright (C) 2008 - 2010 jdkbx nfc@jdkbx.cjb.net
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package nfc;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;

import javax.bluetooth.BluetoothStateException;
import javax.bluetooth.DataElement;
import javax.bluetooth.DeviceClass;
import javax.bluetooth.DiscoveryAgent;
import javax.bluetooth.DiscoveryListener;
import javax.bluetooth.L2CAPConnection;
import javax.bluetooth.L2CAPConnectionNotifier;
import javax.bluetooth.LocalDevice;
import javax.bluetooth.RemoteDevice;
import javax.bluetooth.ServiceRecord;
import javax.bluetooth.UUID;
import javax.microedition.contactless.ContactlessException;
import javax.microedition.contactless.DiscoveryManager;
import javax.microedition.contactless.TargetListener;
import javax.microedition.contactless.TargetProperties;
import javax.microedition.contactless.TargetType;
import javax.microedition.contactless.sc.ISO14443Connection;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;
import javax.microedition.io.StreamConnectionNotifier;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.Display;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.List;
import javax.microedition.lcdui.TextField;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;


/**
 * Midlet to run on the NFC (JSR-257) device.
 * @author jdkbx
 *
 */
public class BluetoothNFCReader extends MIDlet implements TargetListener,
		CommandListener, DiscoveryListener {

	/** global display reference for easier access. */
	private Display display;
	/** Form displayed initially. */
    private Form formStart;
    /** Form displayed during Bluetooth service publishing. */
    private Form formServerStart;
    /** Form displayed when waiting for RFID card to enter Reader field. */
    private Form formConnect;
    /** Form displayed when discovering bluetooth devices. */
    private Form formDiscover;
    /** Form showing APDUs and RAPDUS info. */
    private Form formReader;
    /** Form for displaying more info about state. */
    private Form formLog;
    /** Form where errors will be made visible. */
    private Form formError;
    /** Form where infos will be displayed. */
    private Form formAbout;
    /** Displays last received APDU (full). */
    private TextField tfIn;
    /** Displays last send RAPDU (full). */
    private TextField tfOut;
    /** Displays last received APDU (partly). */
    private TextField tfReadIn;
    /** Displays last send RAPDU (partly). */
    private TextField tfWriteOut;
    //private TextField msgTf;
    /** Abort and go back to start.*/
    private Command cmdCancel;
    /** Discover visible bluetooth devices. */
    private Command cmdDiscover;
    /** Show info on discovered bluetooth device. */
    private Command cmdSelect;
    /** Exit midlet. */
    private Command cmdExit;
    /** Start the Reader Emulation. */
    private Command cmdServer;
    /** Show Log. */
    private Command cmdLog;
    /** Go back to previous Form. */
    private Command cmdBack;
    /** Remove Card from Reader. */
    private Command cmdRemove;
    /** Show infos. */
    private Command cmdAbout;
    /** Show infos. */
    private Command cmdExtSend;
    
    /** uid from RFID card. */
    private String uid;
    
    /** currently selected target type. cards or tags.*/
	private TargetType currTargetType = null;

	/** connection to use for card communication.*/
	private ISO14443Connection isoConn = null;
	
	/** to discover bluetooth devices. */
    private DiscoveryAgent discoveryAgent;
    /** Holds discovered bluetooth devices. */
    private Vector devices;
    /** Lists discovered devices. */
    private List deviceList;
    
    /** Bluetooth RFCOMM connection abstraction. */
    private L2CAPConnection connectionL2CAP;

    private StreamConnection connectionRFCOMM;
    
    boolean USE_L2CAP = false;
    boolean USE_FRAGMENTATION = true;
    
    //UUID SPP_UUID = new UUID("1101", true);
    /** Service UUID. */
    private static final long SRV_UUID = 0x0100;
    
    UUID SPP_UUID = new UUID("1101", true);
    
    /** actual Service name. */
    private static final String SRV_NAME = "SPPServer1";
    /** url needed to publish service. */
    private String serviceURLL2CAP =
    	"btl2cap://localhost:" + new UUID(SRV_UUID).toString()
    	+ ";name=" + SRV_NAME;
    private String serviceURLRFCOMM =
    	"btspp://localhost:" + SPP_UUID
    	+ ";name=" + SRV_NAME;
    
    /** holds APDU communication. */
    private byte[] buffer;
    
    /** flag to list bluetooth devices by name. */
    private static final int SERVICE_NAME_ATTRID = 0x0100;
    
    /** whether a RFID card has been detected and not yet removed by cmd. */
    private boolean isCardPresent = false;
    
    /** whether the driver is waiting for an card detected event. */
    private boolean isWaitingEvent = false;
    
    /** Whether logging is enabled. */
    private boolean LOG = true;
    
    /** holds the reader functionality. */
    private Reader r = new Reader();
    
    /** half byte mask for convertion. */
    private static final short HALF_BYTE = 0xF;
    
	/* (non-Javadoc)
	 * @see javax.microedition.midlet.MIDlet#destroyApp(boolean)
	 */
	protected void destroyApp(final boolean arg0)
	throws MIDletStateChangeException { }

	/* (non-Javadoc)
	 * @see javax.microedition.midlet.MIDlet#pauseApp()
	 */
	protected void pauseApp() { }

	/* (non-Javadoc)
	 * @see javax.microedition.midlet.MIDlet#startApp()
	 */
	protected final void startApp() throws MIDletStateChangeException {
        display = Display.getDisplay(this);

        initCommands();      
        initForms();
        
        display.setCurrent(formStart);
        uid = "";
	}
	
	/**
	 * Prepares cmds.
	 */
	private void initCommands() {
	  cmdCancel = new Command("Cancel", Command.CANCEL, 1);
      cmdDiscover = new Command("Discover", Command.ITEM, 1);
      cmdSelect = new Command("Select", Command.OK, 1);
      cmdExit = new Command("Exit", Command.EXIT, 1);
      cmdServer = new Command("Server", Command.ITEM, 1);
      cmdLog = new Command("Log", Command.ITEM, 1);
      cmdBack = new Command("Back", Command.BACK, 1);
      cmdRemove = new Command("Remove", Command.ITEM, 1);
      cmdAbout = new Command("About", Command.ITEM, 1);
      cmdExtSend = new Command("send ext APDU", Command.ITEM, 1);
	}
	
	/**
	 * Prepares forms.
	 */
	private void initForms() {
	  formStart = new Form("Bluetooth NFC Reader");   
      formServerStart = new Form("Server");
      formConnect = new Form("Connect");
      formDiscover = new Form("Discover");
      formReader = new Form("Reader");
      formLog = new Form("Log");
      formError = new Form("Error");
      formAbout = new Form("About");
      tfIn = new TextField("Last APDU", "", 520, TextField.UNEDITABLE);
      tfOut = new TextField("Last RAPDU", "", 520, TextField.UNEDITABLE);
      tfReadIn = new TextField("APDU size", "", 25, TextField.UNEDITABLE);
      tfWriteOut = new TextField("RAPDU size and responsecode", "", 12, TextField.UNEDITABLE);
      setupForms();
	}
	
	/**
	 * Clears all Forms.
	 */
	private void setupForms() {
	  resetFormStart();
      resetFormServerStart();
      resetFormConnect();      
      resetFormDiscover();
      resetFormReader();
      resetFormLog();
      resetFormError();
      resetFormAbout();
	}
	
	/**
	 * Clears formAbout.
	 */
	private void resetFormAbout() {
	  formAbout.deleteAll();
	  formAbout.setTitle("About");
	  formAbout.addCommand(cmdBack);
	  formAbout.addCommand(cmdExit);
	  formAbout.setCommandListener(this);
	  formAbout.append("receiveMTU: " + LocalDevice.getProperty("bluetooth.l2cap.receiveMTU.max") + "\n");
	  formAbout.append("(c) Copyright jdkbx 2009.  All rights reserved.\nnfc@jdkbx.cjb.net");
    }
	
	/**
	 * Clears formError.
	 */
	private void resetFormError() {
      formError.deleteAll();
      formError.setTitle("Error");
      formError.addCommand(cmdCancel);
      formError.addCommand(cmdExit);
      formError.addCommand(cmdLog);
      formError.setCommandListener(this);
    }
	
	/**
	 * Clears form.
	 */
	private void resetFormServerStart() {
	  formServerStart.deleteAll();
	  formServerStart.setTitle("Server");
//      msgTf = new TextField("msg:", "", 256, TextField.ANY);
//      formDisplay.append(msgTf);
      formServerStart.addCommand(cmdCancel);
      formServerStart.setCommandListener(this);
	}
	
	/**
	 * Clears form.
	 */
	private void resetFormConnect() {
	  formConnect.deleteAll();
	  formConnect.setTitle("Connect");
	  formConnect.append("\n\n connect card!");
      formConnect.addCommand(cmdCancel);
      formConnect.setCommandListener(this);
	}
	
	/**
	 * Clears form.
	 */
	private void resetFormDiscover() {
	  formDiscover.deleteAll();
	  formDiscover.setTitle("Discover");
      formDiscover.addCommand(cmdCancel);
      formDiscover.setCommandListener(this);
	}
	
	/**
	 * Clears form.
	 */
	private void resetFormReader() {
	  formReader.deleteAll();
	  formReader.setTitle("Reader");
	  tfReadIn.setString("");
	  tfWriteOut.setString("");
	  formReader.append(tfReadIn);
	  formReader.append(tfWriteOut);
      formReader.addCommand(cmdCancel);
      if (LOG)
          formReader.addCommand(cmdLog);
      formReader.addCommand(cmdRemove);
      if (LOG)
          formReader.addCommand(cmdExtSend);
      formReader.setCommandListener(this);
	}
	
	/**
	 * Clears form.
	 */
	private void resetFormStart() {
	  formStart.deleteAll();
	  formStart.setTitle("Bluetooth Reader");
      formStart.addCommand(cmdServer);
      formStart.addCommand(cmdDiscover);
      formStart.addCommand(cmdAbout);
      formStart.addCommand(cmdExit);  
      formStart.setCommandListener(this);
	}
	
	/**
	 * Clears form.
	 */
	private void resetFormLog() {
	  formLog.deleteAll();
	  formLog.setTitle("Log");
	  tfIn.setString("");
	  tfOut.setString("");
	  formLog.append(tfIn);
	  formLog.append(tfOut);
      formLog.addCommand(cmdBack);
      formLog.setCommandListener(this);
	}

	/**
	 * Converts raw bytes into Hexstring so it can be displayed.
	 * @param value raw input
	 * @param offset into value
	 * @param len length of input to convert from offset
	 * @return converted String, contains "null" on error
	 */
    private static String toHexString(final byte[] value, final int offset,
    		final int len) {
        if (value == null) {
            return "null";
        }
        if (len < 1 || offset < 0 || offset + 1 > value.length) {
        	return "null";
        }
        
        StringBuffer result = new StringBuffer();

        for (int i = 0; i + offset < value.length && i < len; i++) {
            result.append(Integer.toHexString(
            		(value[i + offset] >>> 4) & HALF_BYTE).toUpperCase());
            result.append(Integer.toHexString(
            		value[i + offset] & HALF_BYTE).toUpperCase());
        }
        return result.toString();
    }

//    /**
//     * Convert HexString into raw bytes. i guess
//     * @param string input
//     * @return converted input
//     */
//    private static byte[] toByteArray(final String string) {
//        if (string.length() % 2 != 1) {
//            byte[] result = new byte[string.length() / 2];
//            for (int i = 0; i < result.length; i++) {
//                result[i] = (byte) Integer.parseInt(string.substring(2 * i,
//                        2 * i + 2), 16);
//            }
//            return result;
//        } else {
//            return null;
//        }
//    }
	

	/* (non-Javadoc)
	 * @see javax.microedition.lcdui.CommandListener#commandAction(javax.microedition.lcdui.Command, javax.microedition.lcdui.Displayable)
	 */
	public final void commandAction(final Command cmd,
			final Displayable displayForm) {

        if (displayForm == formStart) {
            if (cmd == cmdDiscover) {
                formStart.append("starting client\n");
                btSearch();
            } else if (cmd == cmdServer) {
            	formStart.append("starting server\n");
            	startServer();
            } else if (cmd == cmdAbout) {
            	resetFormAbout();
            	display.setCurrent(formAbout);
            }
        } else if (displayForm == formAbout) { 
        	if (cmd == cmdBack) {
                resetFormStart();
                display.setCurrent(formStart);
            }
        } else if (displayForm == deviceList) { 
        	if (cmd == cmdCancel) {
                discoveryAgent.cancelInquiry(this);
                resetFormStart();
                display.setCurrent(formStart);
            } else if (cmd == cmdExit) {
                notifyDestroyed();
            } else if (cmd == cmdSelect) {
            	if (deviceList.getSelectedIndex() != deviceList.size() - 1) {
	            	 RemoteDevice btDevice = (RemoteDevice) devices
	                 .elementAt(deviceList.getSelectedIndex());
	            	startClient(btDevice);
            	}
            }
        } else if (displayForm == formError) {
            if (cmd == cmdCancel) {
                disconnectCard(formError, true);
                abort();
            } else if (cmd == cmdLog) {
              display.setCurrent(formLog);
            }
        } else if (displayForm == formReader) {
            if (cmd == cmdCancel) {
                disconnectCard(formError, true);
                abort();
            } else  if (cmd == cmdLog) {
                display.setCurrent(formLog);
            } else if (cmd == cmdRemove) {
              disconnectCard(formError, true);
              startSmartCardDiscovery();
              resetFormError();
              resetFormReader();
              resetFormConnect();
              resetFormLog();
              display.setCurrent(formConnect);
            } else if (cmd == cmdExtSend) {
                try {
                	byte[] resp = isoConn.exchangeData(new byte[] {0x0,(byte)0xb0, (byte)0x9c,0x0,0x0,0x0,(byte)0xf0});
                	if (resp != null && resp.length > 0) {
                		formLog.append("recieved RAPDU with length " + resp.length + "\n");
                	} else {
                		formLog.append("failed to get extended RAPDU\n");
                	}
                } catch (ContactlessException e) {
                	formLog.append(e.getMessage() + "\n");
                	formLog.append("ContactlessException while sending 00b09c000000f0\n");
                } catch (IOException e) {
                	formLog.append(e.getMessage() + "\n");
                	formLog.append("IOException while sending 00b09c000000f0\n");
                }
              }
        } else if (displayForm == formLog) {
            if (cmd == cmdBack) {
                display.setCurrent(formReader);
            } else if (cmd == cmdCancel) {
                disconnectCard(formError, true);
                abort();
            }
        } else if (displayForm == formServerStart) {
            if (cmd == cmdCancel) {
            	abort();
            }
        } else if (displayForm == formConnect) {
            if (cmd == cmdCancel) {
                abort();
	        }
	    }
        //else if (displayForm == formDisplay) {
//        	if (cmd == cancelCmd) {
//        		display.setCurrent(form);
//        		form.addCommand(discoverCmd);
//        	}
//        } else 
		if (cmd == cmdExit) {
        	try {
				destroyApp(true);
			} catch (MIDletStateChangeException e) {
				e.printStackTrace();
			}
			notifyDestroyed();
			return;
        }
	}
	
	/**
	 * Aborts reader thread, kills bluetooth connection and returns to start.
	 */
	private void abort() {
		resetFormStart();
		isWaitingEvent = false;
        r.kill();
        if (connectionL2CAP != null) {
        	try {
				connectionL2CAP.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
        	connectionL2CAP = null;
        }
        if (connectionRFCOMM != null) {
        	try {
        		connectionRFCOMM.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			connectionRFCOMM = null;
        }
        display.setCurrent(formStart);
	}

	/**
	 * Publish Reader Service and wait for incoming connection.
	 * Afterwards initiate RFID card stuff.
	 */
	private void startServer() {
	    resetFormServerStart();
		formServerStart.append(">>starting service\n");
		display.setCurrent(formServerStart);
		try {
			LocalDevice btDevice = LocalDevice.getLocalDevice();
			btDevice.setDiscoverable(DiscoveryAgent.GIAC);

			if (USE_L2CAP == true) {
				L2CAPConnectionNotifier notifier =
					(L2CAPConnectionNotifier) Connector.open(serviceURLL2CAP);
				formServerStart.append(">>waiting for connection on\n"
						+ serviceURLL2CAP + "\n");
	//			formServerStart.wait();
				connectionL2CAP = notifier.acceptAndOpen();
	//			RemoteDevice rDev = RemoteDevice.getRemoteDevice(connection);
	//			formServerStart.append(">>remote address: "
	//					+ rDev.getBluetoothAddress() + "\n");
	//			formServerStart.append(">>remote name: "
	//					+ rDev.getFriendlyName(true) + "\n");
			} else {
				StreamConnectionNotifier notifier2 =
					(StreamConnectionNotifier) Connector.open(serviceURLRFCOMM);
				formServerStart.append(">>waiting for connection on\n"
						+ serviceURLL2CAP + "\n");
	//			formServerStart.wait();
				connectionRFCOMM = notifier2.acceptAndOpen();
	//			RemoteDevice rDev = RemoteDevice.getRemoteDevice(connection);
	//			formServerStart.append(">>remote address: "
	//					+ rDev.getBluetoothAddress() + "\n");
	//			formServerStart.append(">>remote name: "
	//					+ rDev.getFriendlyName(true) + "\n");
			}
			
			startSmartCardDiscovery();
			
			// start thread
			Thread reader = new Thread(r);
			reader.start();
//			formServerStart.append("started reader\n");
		} catch (BluetoothStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
//		formServerStart.append(">>done\n");
	}
	
	/**
	 * Asks for a RFID card to be connected.
	 */
	private void startSmartCardDiscovery() {
		resetFormConnect();
		display.setCurrent(formConnect);
		initializeCardDiscManager();
	}

	/**
     * Removes targetListener for ISO cards. 
     */
    private void removeCardDiscManager() {
        currTargetType = TargetType.ISO14443_CARD;
        DiscoveryManager dm = DiscoveryManager.getInstance();
        try {
            dm.removeTargetListener(this, TargetType.ISO14443_CARD);
        } catch (IllegalStateException e1) {
            e1.printStackTrace();
        }
    }
    
	/**
	 * Adds Callback for ISO cards. 
	 */
	private void initializeCardDiscManager() {
		currTargetType = TargetType.ISO14443_CARD;
		DiscoveryManager dm = DiscoveryManager.getInstance();
		try {
			dm.removeTargetListener(this, TargetType.ISO14443_CARD);
		} catch (IllegalStateException e1) {
			e1.printStackTrace();
		}
		try {
			dm.addTargetListener(this, TargetType.ISO14443_CARD);
			return;

		} catch (IllegalStateException e1) {
			e1.printStackTrace();
		} catch (ContactlessException e1) {
			e1.printStackTrace();
		}
	}
	
	/**
	 * Interface callback, when ICC is detected by reader.
	 * @param target Properties of the detected ICC
	 */
	public final void targetDetected(final TargetProperties[] target) {
		if (currTargetType.equals(TargetType.ISO14443_CARD)) {
			cardDetected(target);
		}
	}
	
	/**
	 * Handle a detected smartcard.
	 * @param target connection
	 */
	private void cardDetected(final TargetProperties[] target) {
		if (!connectExternalCard(target, formConnect)) {
		    display.setCurrent(formError);
			return;
		}
		// show reader form
		isCardPresent = true;
		if (isWaitingEvent) {
			// TODO send event command
		}
		display.setCurrent(formReader);
	}
	
	/**
	 * Establishes connection to external card.
	 * @param target to make connection
	 * @param currentForm where to display messages
	 * @return success status
	 */
	private boolean connectExternalCard(final TargetProperties[] target,
			final Form currentForm) {
		Class[] classes = target[0].getConnectionNames();
		
		for (int j = 0; j < classes.length; ++j) {
			try {
				if (isoConn != null) {
					disconnectCard(currentForm, false);
				}
				if (classes[j].equals(Class.forName(
					"javax.microedition.contactless.sc.ISO14443Connection"))) {

					String url = target[0].getUrl(classes[j]);
			
					try {
						isoConn = (ISO14443Connection) Connector.open(url);
					} catch (IOException e) {
						currentForm.append("IOException\n");
						return false;
					}
					
					uid = target[0].getUid();
					currentForm.append("connected\n");
					return true;
				}
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		
		return false;
	}
	
	/**
	 * Closes connection.
	 * @param currentForm where to display messages.
	 * @param removeTargetListener whether to still react on cards
	 * @return true if successful
	 */
	private boolean disconnectCard(final Form currentForm,
			final boolean removeTargetListener) {
		isCardPresent = false;
		isWaitingEvent = false;
		try {
			if (isoConn != null) {
				isoConn.close();
			}
		} catch (IOException e) {
			currentForm.append("IOException\n");
			return false;
		}
		if (removeTargetListener) {
		  removeCardDiscManager();
		}
		return true;
	}
	
	/**
	 * Starts bluetooth service discovery.
	 * @param btDevice where to query sdp
	 */
	private void startClient(final RemoteDevice btDevice) {
	    resetFormServerStart();
		display.setCurrent(formServerStart);
		formServerStart.append(">>trying to discover remote services\n");
		try {
			LocalDevice localDevice = LocalDevice.getLocalDevice();
			DiscoveryAgent agent = localDevice.getDiscoveryAgent();
			UUID[] uuids = new UUID[] {new UUID(SRV_UUID)};
			int[] attrSet = {SERVICE_NAME_ATTRID};
			int id = agent.searchServices(attrSet, uuids, btDevice, this);
			formServerStart.append(">>got service: " + id + "\n");
		} catch (BluetoothStateException e) {
			e.printStackTrace();
		}
	}
	
    /**
     * Starts the search for the Bluetooth devices.
     */
    private void btSearch() {

        // Set the right UI
        deviceList = new List("Searching devices", List.IMPLICIT);
        deviceList.setSelectCommand(cmdSelect);
        deviceList.addCommand(cmdCancel);
        deviceList.setCommandListener(this);
        display.setCurrent(deviceList);

        devices = new Vector();
        // Start an inquiry to find Bluetooth devices
        try {
            LocalDevice local = LocalDevice.getLocalDevice();
            discoveryAgent = local.getDiscoveryAgent();
            discoveryAgent.startInquiry(DiscoveryAgent.GIAC, this);

        } catch (BluetoothStateException e) {
          formLog.append(e.getMessage());
        }
    }
    
	/* (non-Javadoc)
	 * @see javax.bluetooth.DiscoveryListener#deviceDiscovered(javax.bluetooth.RemoteDevice, javax.bluetooth.DeviceClass)
	 */
	public final void deviceDiscovered(final RemoteDevice btDevice,
			final DeviceClass cod) {
		// When device has been found add it to devices Vector..
        devices.addElement(btDevice);

        // .. and to the deviceList List
        String devName;
        try {
            devName = btDevice.getFriendlyName(false);

            if (devName == null || devName.compareTo("") == 0) {
                devName = btDevice.getBluetoothAddress();
            }
            deviceList.append(devName, null);
        } catch (IOException e) {
        	e.printStackTrace();
        }
	}

	/* (non-Javadoc)
	 * @see javax.bluetooth.DiscoveryListener#inquiryCompleted(int)
	 */
	public final void inquiryCompleted(final int status) {
		// When searching of devices has completed set title to indicate that
	    if (status == INQUIRY_COMPLETED) {
            deviceList.setTitle("Inquiry completed");
	    } else if (status == INQUIRY_TERMINATED) {
	        deviceList.setTitle("Inquiry terminated");
	    } else if (status == INQUIRY_ERROR) {
            deviceList.setTitle("Inquiry error");
        }
        
	    // add local adapter, so own btaddress can be displayed
        try {
			LocalDevice local = LocalDevice.getLocalDevice();
			String name = "local: "; //local.getFriendlyName();
			//if (name == null || name.compareTo("") == 0) {
				name +=  local.getBluetoothAddress();
			//}
			devices.addElement(local);
			deviceList.append(name, null);
		} catch (BluetoothStateException e) {
			e.printStackTrace();
		}
		synchronized (this) {
		    try { this.notifyAll(); } catch (Exception e) {
		    	e.printStackTrace();
		    }
		}
	}
	
	/* (non-Javadoc)
	 * @see javax.bluetooth.DiscoveryListener#serviceSearchCompleted(int, int)
	 */
	public final void serviceSearchCompleted(final int transID,
			final int respCode) {
		if (respCode != SERVICE_SEARCH_COMPLETED) {
			formServerStart.append(">>service discovery failed "
					+ respCode + "\n");
			return;
		}
		
		formServerStart.append(">>service discovery done " + transID + " "
				+ respCode + "\n");
		
//		if (serviceURL == null) {
//			formServerStart.append("!!url empty, abort\n");
//			return;
//		}
		
		synchronized (this) {
            try { this.notifyAll(); } catch (Exception e) {
            	e.printStackTrace();
            }
        }
		
//		formServerStart.append(">>connecting to service: " + serviceURL);
//		
//		try {
//			connection = (StreamConnection) Connector.open(serviceURL);
//			startSmartCardDiscovery();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
	}
	
	/* (non-Javadoc)
	 * @see javax.bluetooth.DiscoveryListener#servicesDiscovered(int, javax.bluetooth.ServiceRecord[])
	 */
	public final void servicesDiscovered(final int transID,
			final ServiceRecord[] records) {
		formServerStart.append(">>services discovered #: "
				+ records.length + "\n");
		
		for (int i = 0; i < records.length; ++i) {
			String serviceURL = records[i].getConnectionURL(
					ServiceRecord.NOAUTHENTICATE_NOENCRYPT, false);
			DataElement serviceNameElement =
				records[i].getAttributeValue(SERVICE_NAME_ATTRID);
			
			if (serviceNameElement != null) {
    			String name = (String) serviceNameElement.getValue();
    			formServerStart.append(">>" + name + "\n");
    			
    			if (serviceURL.startsWith("btspp")
    					&& name.indexOf(SRV_NAME) > -1) {
    				formServerStart.append(">>got btspp service " + i + "\n");
    				break;
    			}
			}
		}
	}

	/**
	 * The NFC connection reader emulation functionality.
	 * @author jdkbx
	 *
	 */
	public class Reader extends Thread {
		/** limit commands to a length of 2 bytes. */
	  	private static final int READER_CMD_LENGTH = 2;
	  	/** cmd to return card ATR/ATS. */
	    private final byte[] GET_ATTR_CMD = new byte[] {0x20, 0x21};
	    /** cmd to ask if a ICC is currently present. */
	    private final byte[] GET_PRESENCE_CMD = new byte[] {0x20, 0x22};
	    /** cmd to kill the thread. */
	    private final byte[] END_BYTES = new byte[] {0x20, 0x23};
	    /** cmd to enable event when card is detected. */
	    private final byte[] CARD_EVT_CMD = new byte[] {0x20, 0x24};
	    /** did we receive an end cmd? */
	    private boolean end = false;
	    /** constant to check with 0x00. */
	    private final byte[] NULL_BYTES = new byte[] {0x00};
	    /** constant to check for 0x01. */
	    private final byte[] ONE_BYTES = new byte[] {(byte) 1};
	    /** buffer size limit. */
	    private static final short BUFFER_SIZE = 32767;
	    /** max APDU size. */
	    private static final short MAX_APDU_SIZE = BUFFER_SIZE - 2;
	    /** byte mask for converting 0xFF. */
	    private static final int BYTE_MASK = 0xFF;
	    
	    /**
	     * Order the thread to abort at the next clean possibility.
	     */
	    public final void kill() {
	    	end = true;
	    }
	
	    /**
	     * Entry point.
	     */
	    public final void run() {
	    	formLog.append("started\n");
	    	end = false;
	    	startReader();
	    }
	    
	    /**
	     * Starts the main reader loop.
	     */
	    private void startReader() {
	        resetFormReader();
	        formReader.append(" -- communicating -- \n");
	        //display.setCurrent(formReader);
	            
	        buffer = new byte[BUFFER_SIZE];
	        byte[] cardResponse;
	        byte[] response;
	        //byte[] responseLength = new byte[2];
	        
	        OutputStream os = null;
            InputStream is = null;
            
	        try {
	        	if (USE_L2CAP == false) {
	                os = connectionRFCOMM.openOutputStream();
	                is = connectionRFCOMM.openInputStream();
	        	}
	                
	            int realLen, readLen, offset, len = 0;
	            short hangcheck = 0;
	            
	            // reader loop
	            while (!end) {
	            	realLen = -1;
	                readLen = 0;
	                len = 0;
	                hangcheck = 0;
	                
	                if (USE_L2CAP == true) {
		                // get input (this loop is probably completely useless) 
		                while (readLen != realLen) {
		                	if (hangcheck > 0)
		                		formLog.append("hangcheck " + hangcheck + "\n");
		                    len = connectionL2CAP.receive(buffer);
	//	                    , realLen + 1,
	//	                    		MAX_APDU_SIZE - readLen);
	
		                    hangcheck++;
		                    if (len < 2) {
		                    	try {
		                    		sleep(10);
		                    	} catch (InterruptedException e) {
			                        formReader.append("ending\n");
			                        return;
		                    	}
			                    continue;
			                } else {
			                	if (realLen < 1) {
			                        realLen = (int) (buffer[0] & BYTE_MASK) << 8;
			                        realLen += (int) (buffer[1] & BYTE_MASK);
			                        readLen += len - 2;
			                    } else {
			                        readLen += len;
			                    }
			                }
			            }
	                } else {
	                	// TODO error checking
	                	len = is.read(buffer);
	                	if (realLen < 1) {
	                        realLen = (int) (buffer[0] & BYTE_MASK) << 8;
	                        realLen += (int) (buffer[1] & BYTE_MASK);
	                        readLen += len - 2;
	                    }
	                	
	                	if (LOG && readLen != realLen)
	                		formLog.append("error len in apdu:" + realLen + " recv:" + readLen + "\n");
	                	
	                	if (LOG && readLen > 255)
	                		formLog.append("extended APDU len: " + readLen + "\n");
	                }
	                                  
	              	len = readLen;
	              	offset = 2;
//                  byte[] cmd = new byte[len];
//                  for (int i = 0 ; i < readLen; ++i) {
//                      cmd[i] = buffer[i + 1];
//                  }
	                
	              	// write messages to screen
	                if (len > 2) { // && len != READER_CMD_LENGTH) {
	                    tfReadIn.setString(">> " + len + "\n" 
	                    		+ toHexString(buffer, 2, 8));
	                    tfWriteOut.setString(" \n ");
	                }
	                
	                if (LOG) {
		                // log APDU
		                tfIn.setString(toHexString(buffer, 2, len > 255 ? 255 : len));
		                tfOut.setString(" \n ");
	                }
	            	                    
	                // handle cmds  
	                if (len == READER_CMD_LENGTH) {
	                	//formLog.append("might have gotten cmd\n");
	                    
	                	// end from remote
	                	if (isEnd(buffer, offset, len)) {
	                        end = true;
	                        formReader.append(">>end command\n");
	                        formLog.append(">>end command\n");
	                        break;
	                  	}
	                    
	                	// ATR/ATS request, will just write uid
	                    if (equal(GET_ATTR_CMD, buffer, len)) {
	                        //formLog.append(">>atr\n");
	                        response = new byte[uid.length() + 2];
	                        response[0] = (byte) (BYTE_MASK & (uid.length() >> 8));
	                        response[1] = (byte) (BYTE_MASK & uid.length());
	                        for (int j = 0; j < uid.length(); ++j) {
	                        	response[j + 2] = uid.getBytes()[j];
	                        }
	                        if (USE_L2CAP == true) {
	                        	connectionL2CAP.send(response);
	                        } else {
	                        	os.write(response);
	                        	os.flush();
	                        }
	                        continue;
	                    }
	                      
	                    /**
	                     * presence polling from remote.
	                     * answer with only 1 byte.
	                     * 0x1 if ICC available, 0x0 otherwise
	                     */
	                    if (equal(GET_PRESENCE_CMD, buffer, len)) {
	                        //formLog.append(">>presence\n");
	                    	response = new byte[3];
	                        response[0] = (byte) 0;
	                        response[1] = (byte)  1;
	                        if (isCardPresent) {
	                        	response[2] = ONE_BYTES[0];
	                        } else {
	                        	response[2] = NULL_BYTES[0];
	                        }
	                        if (USE_L2CAP == true) {
	                        	connectionL2CAP.send(response);
	                        } else {
	                        	os.write(response);
	                        	os.flush();
	                        }
	                        continue;
	                    }
	                    
	                    /**
	                     * presence event tracking from remote.
	                     * only when a target will be acquired, a response will be send
	                     */
	                    if (equal(CARD_EVT_CMD, buffer, len)) {
	                        //formLog.append(">>card_event\n");
	                    		                    	
	                        if (isCardPresent) {
	                        	response = new byte[3];
	                        	response[0] = (byte) 0;
		                        response[1] = (byte) 1;
		                        response[2] = ONE_BYTES[0];
		                        if (USE_L2CAP == true) {
		                        	connectionL2CAP.send(response);
		                        } else {
		                        	os.write(response);
		                        	os.flush();
		                        }
	                        	isWaitingEvent = false;
	                        } else {
	                        	isWaitingEvent = true;
	                        }                      
	                        continue;
	                    }
	                      
	                    //formLog.append("matched no cmd\n");
	                //} else {
	                  	//formLog.append("doing real shit\n");
	                }

	                if (isCardPresent) {
	                	cardResponse = null;
	                	// relay APDU to card
	                    cardResponse = sendAPDU(buffer, offset, len);
	                        
	                    if (cardResponse != null && cardResponse.length > 0) {
	                    	
	                    	response = null;
	                    	if (USE_L2CAP == true) {
	                    		if (USE_FRAGMENTATION == true) {
			                    	if (cardResponse.length <= 48 - 2) {
				                    	response = new byte[cardResponse.length + 2];
				                    	response[0] = 0;
				                    	response[1] =
				                    		(byte) (BYTE_MASK & cardResponse.length);
			
				                    	for (int j = 0; j < cardResponse.length; ++j) {
				                    		response[j + 2] = cardResponse[j];
				                    	}
				                    	
				                    	connectionL2CAP.send(response);
			                    	} else {
			                    		int cnt = ((cardResponse.length + 2) / 48) + 1;
			                    		response = new byte[48];
			                    		response[0] = 
			                    			(byte) (BYTE_MASK & (cardResponse.length >> 8));
			                    		response[1] =
				                    		(byte) (BYTE_MASK & cardResponse.length);
			                    		
			                    		for (int j = 0; j < 46; ++j) {
				                    		response[j + 2] = cardResponse[j];
				                    	}
			                    		
			                    		connectionL2CAP.send(response);
			                    		int k = 1;
			                    		while (k < cnt) {
			                    			if (k == cnt - 1) {
			                    				response = new byte[(cardResponse.length % 48) + 2];
			                    			}
			                    			for (int j = 0; j < 48 && j + ((48 * k) - 2) < cardResponse.length; ++j) {
					                    		response[j] = cardResponse[j + ((48 * k) - 2)];
					                    	}
				                    		
				                    		connectionL2CAP.send(response);
				                    		++k;
			                    		}
			                    	}
	                    		} else {
	                    			response = new byte[cardResponse.length + 2];
	                    			response[0] = 
		                    			(byte) (BYTE_MASK & (cardResponse.length >> 8));
		                    		response[1] =
			                    		(byte) (BYTE_MASK & cardResponse.length);
		
			                    	for (int j = 0; j < cardResponse.length; ++j) {
			                    		response[j + 2] = cardResponse[j];
			                    	}
			                    	
			                    	connectionL2CAP.send(response);
	                    		}
		                    } else {
		                    	response = new byte[cardResponse.length + 2];
		                    	response[0] = 
	                    			(byte) (BYTE_MASK & (cardResponse.length >> 8));
	                    		response[1] =
		                    		(byte) (BYTE_MASK & cardResponse.length);
	                    		for (int j = 0; j < cardResponse.length; ++j) {
		                    		response[j + 2] = cardResponse[j];
		                    	}
		                    	os.write(response);
		                    	os.flush();
		                    }
	                        
	                        // log response
	                    	if (cardResponse.length >= 2) {
		                        tfWriteOut.setString("<< " + cardResponse.length 
		                        		+ "\n" + toHexString(cardResponse,
		                        				cardResponse.length - 2, 2));
	                    	} else {
	                    		tfWriteOut.setString("<< " + cardResponse.length 
		                        		+ "\n" + toHexString(cardResponse, 0, 1));
	                    	}
	                    	if (LOG) {
		                    	if (response.length > 0) {
			                        tfOut.setString(toHexString(
			                        		response, 0, response.length > 255 ? 255: response.length));
		                    	} else {
		                    		tfOut.setString(toHexString(
			                        		response, 0, response.length));
		                    	}
	                    	}
	                    } else {
	                    	if (USE_L2CAP == true) {
	                    		connectionL2CAP.send(NULL_BYTES);
	                    	} else {
	                    		os.write(NULL_BYTES);
	                    		os.flush();
	                    	}
	                    }
	                } else {
	                	// this should never happen
	                    //formLog.append("nuthing done\n");
	                	if (USE_L2CAP == true) {
                    		connectionL2CAP.send(NULL_BYTES);
                    	} else {
                    		os.write(NULL_BYTES);
                    		os.flush();
                    	}
	                }
	            } // end of reader loop	            
	        } catch (IOException e) {
	            e.printStackTrace();
	            formError.append(e.getMessage());
	            formError.append("!!Bluetooth error\nrestart necessary\n");
	            disconnectCard(formError, true);
	            display.setCurrent(formError);
	            return;
	        }
	        if (connectionL2CAP != null) {
		        try {
		            connectionL2CAP.close();
		        } catch (IOException e) {
		            e.printStackTrace();
		        }
		        connectionL2CAP = null;
	        }
	        if (connectionRFCOMM != null) {
		        try {
		        	if (is != null)
		        		is.close();
		        	if (os != null)
		        		os.close();
		            connectionRFCOMM.close();
		        } catch (IOException e) {
		            e.printStackTrace();
		        }
		        connectionRFCOMM = null;
	        }
	        if (display.getCurrent() == formReader) {
		        resetFormStart(); 
		        display.setCurrent(formLog); //Start);
	        }
	       
	    }
	            
	    /**
	     * Communicates with ICC.
	     * @param apduBuffer APDU
	     * @param offset into apduBuffer
	     * @param len length from offset in apduBuffer
	     * @return RAPDU
	     */
		private byte[] sendAPDU(final byte[] apduBuffer, final int offset,
				final int len) {
			if (apduBuffer == null || apduBuffer.length == 0
					|| len > apduBuffer.length) {
				return NULL_BYTES;
			}
			
	        byte[] cmd = new byte[len];
	        for (int i = 0; i < len; ++i) {
	        	cmd[i] = apduBuffer[i + offset];
	        }
	        
	        try {
	        	
	        	return isoConn.exchangeData(cmd);
	        } catch (IOException e) {
	        	e.printStackTrace();
	        	formError.append(e.getMessage());
	        	formError.append("!!NFC IO error\n");
	        	if (LOG) {
		        	formLog.append(e.getMessage() + "\n");
		        	formLog.append("!!NFC IO error\n");
	        	}
	
	        } catch (ContactlessException e) {
	        	e.printStackTrace();
	        	formError.append(e.getMessage());
	        	formError.append("!!NFC contactless error\nrestart!\n");
	        	if (LOG) {
	        		formLog.append("!!NFC contactless error\n");
	        	}
	        	disconnectCard(formError, true);
	        	display.setCurrent(formError);
	        	r.kill();
	        }
	        return NULL_BYTES;
	    }
	    
		/**
		 * Compares given command with end cmd.
		 * @param cmd command
		 * @param offset offset into cmd
		 * @param length length from offset in cmd
		 * @return whether cmd contains end cmd
		 */
	    private boolean isEnd(final byte [] cmd, final int offset,
	    		final int length) {
	        return equal(END_BYTES, 0, END_BYTES.length, cmd, offset, length);
	    }
	    
	    /**
	     * Compare to byte array for equality.
	     * in the second array an offset of 2 will be used
	     * @param constCmd first
	     * @param second second
	     * @param secondLength length of second
	     * @return true when equal
	     */
	    private boolean equal(final byte[] constCmd, final byte[] second,
	    		final int secondLength) {
	       	return equal(constCmd, 0, constCmd.length, second, 2, secondLength);
	    }
	    
	    /**
	     * Compare to byte arrays for equal content.
	     * @param first first array
	     * @param firstOffset offset into first
	     * @param firstLength length from offset in first
	     * @param second array
	     * @param secondOffset offset into second
	     * @param secondLength length from offset in second
	     * @return true if equal content where specified
	     */
	    private boolean equal(final byte[] first, final int firstOffset,
	    		final int firstLength, final byte[] second,
	    		final int secondOffset, final int secondLength) {
	        if (first == null && second == null) {
	            //formLog.append("both null\n");
	        	return true;
	        }
	        if (first == null || second == null) {
	        	//formLog.append("one null\n");
	        	return false;
	        }
	        if (firstLength < 1 || firstOffset < 0
		        		|| secondLength < 1 || secondOffset < 0) {
	        	return false;
	        }
	        if (firstLength + firstOffset > first.length
		        		|| secondLength + secondOffset > second.length) {
	        	//formLog.append("wrong length specified\n");
	        	return false;
	        }
	        if (firstLength != secondLength) {
//	        	formLog.append("length wrong " + first.length + " "
//	        			+ second.length + "\n");
	        	return false;
	        }
	        for (int i = 0; i < firstLength; ++i) {
	        	if (first[i + firstOffset] != second[i + secondOffset]) {
//	        		formLog.append("different " + i + " " + first[i] + " != "
//	        				+ second[i] + "\n");
	        		return false;
	        	}
	        }
	        return true;
	    }
    }
}
